import { DefinedError } from 'ajv';

export interface AjvErrorParserOptions
{
    json : boolean;
    delimiter : string;
}

function _getAjvErrorMessage(error : DefinedError) : string
{
    // keyword and params are not destructured here because as of now Typescript doesn't
    // interpret the types correctly in the switch statement below.
    const { message, instancePath } = error;
    const cleanedInstancePath = instancePath.slice(1).replace(/\//g, '.');
    // message = message?.toLowerCase();
    let msg = '';
    switch (error.keyword)
    {
        case 'additionalProperties': {
            msg = `${ message } ' ${ error.params.additionalProperty }'`;
            if(cleanedInstancePath.trim())
            {
                msg = `${ msg } in ${ cleanedInstancePath }`;
            }
            break;
        }
        case 'not': {
            msg = cleanedInstancePath.trim();
            if(msg)
            {
                msg += ' is/are ';
            }
            msg += 'not valid';
            break;
        }
        case 'oneOf':
        case 'anyOf': {
            msg = message?.slice(0, -9) ?? '';
            break;
        }
        default: {
            if(!cleanedInstancePath.trim())
            {
                msg = message ?? '';
                break;
            }
            msg = `${ cleanedInstancePath } ${ message }`;
            break;
        }
    }
    return msg;
}

export function parseErrors(
    errors : DefinedError[],
    options : AjvErrorParserOptions = { json: true, delimiter: '\n' }
) : string[] | string
{
    options = { ...{ json: true, delimiter: '\n' }, ...options };
    const messages = errors.map((error) =>
    {
        return _getAjvErrorMessage(error);
    });
    if(options.json)
    {
        return messages;
    }
    return messages.join(options.delimiter);
}
