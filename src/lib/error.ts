export interface RemoteError
{
    code : string,
    message : string,
    name : string,
    stack : string,
    details ?: unknown
}

export class ServerError extends Error
{
    public code = 'SERVER_ERROR';

    constructor(message : string, code ?: string)
    {
        super(message);
        if(code)
        {
            this.code = code;
        }
    }

    public toJSON(excludeStack = false) : Record<string, unknown>
    {
        return {
            name: this.constructor.name,
            message: this.message,
            code: this.code,
            stack: excludeStack ? null : this.stack,
        };
    }
}

export class MalformedWebSocketRequest extends ServerError
{
    public code = 'MALFORMED_REQUEST_ERROR';

    constructor()
    {
        super(`A malformed websocket request was sent.`);
    }
}

export class InvalidRequestPayload extends ServerError
{
    public code = 'INVALID_REQUEST_PAYLOAD';

    constructor(validationFailureReasons : string[])
    {
        super(`An invalid payload was sent. ${ JSON.stringify(validationFailureReasons) }`);
    }
}

export class InvalidContextOperation extends ServerError
{
    public code = 'INVALID_CONTEXT_OPERATION';

    constructor(serviceName : string, context : string, operation : string)
    {
        super(`${ context }.${ operation } is invalid for ${ serviceName }.`);
    }
}

export class RemoteServiceError extends ServerError
{
    public code = 'MALFORMED_REQUEST_ERROR';
    public details ?: unknown;

    constructor(error : RemoteError)
    {
        super(error.message);
        this.code = error.code;
        this.name = error.name;
        this.stack = error.stack;
        this.details = error.details;
    }

    public toJSON(excludeStack = false) : Record<string, unknown>
    {
        return {
            ...super.toJSON(excludeStack),
            details: this.details,
        };
    }
}
