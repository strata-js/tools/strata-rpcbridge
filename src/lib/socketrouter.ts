import { Socket } from 'socket.io';

type SocketRouterHandlerFunction = (socket : Socket, ...args : any[]) => void;

export class SocketRouter
{
    #events = new Map();

    event(eventName : string, handler : SocketRouterHandlerFunction) : void
    {
        this.#events.set(eventName, handler);
    }

    handler(socket : Socket)
    {
        return (event : string, ...args : [any]) : void =>
        {
            const handlerFunction = this.#events.get(event);
            if(handlerFunction)
            {
                args.unshift(socket);
                handlerFunction.apply(this, args);
            }
        };
    }
}

