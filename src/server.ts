import path from 'path';
import dotenv from 'dotenv';
import ConfigUtil from '@strata-js/util-config';

import { ServerConfig } from './interfaces/config.js';
import { RPCBridgeServer } from './rpcbridge.js';

dotenv.config();

// Load config file.
ConfigUtil.load(path.join(process.cwd(), process.env.CONFIG_FILE as string));

const rpcServer = new RPCBridgeServer(ConfigUtil.get<ServerConfig>());
rpcServer.startListening();
