import _ from 'lodash';
import { Ajv, JSONSchemaType } from 'ajv';
import InstallAjvErrors from 'ajv-errors';
import InstallAjvFormats from 'ajv-formats';
import InstallAjvKeywords from 'ajv-keywords';

import { RPCRequest } from '../interfaces/request.js';
import { parseErrors } from '../lib/ajv-error-parser.js';

const SCHEMA_NAME = 'RPC';

const RPCPayloadValidationBase = {
    type: 'object',
    properties: {
        context: { type: 'string', minLength: 1 },
        operation: { type: 'string', minLength: 1 },
        payload: { type: 'object', additionalProperties: true },
        meta: { type: 'object', additionalProperties: true, nullable: true },
        auth: { type: 'string', nullable: true },
        timeout: { type: 'number', minimum: 0, nullable: true },
    },
    additionalProperties: false,
};

const RPCPayloadValidationServiceName = {
    ...RPCPayloadValidationBase,
    properties: {
        ...RPCPayloadValidationBase.properties,
        serviceName: { type: 'string', minLength: 1 },
    },
    required: [ 'serviceName', 'context', 'operation', 'payload' ],
};

const RPCPayloadValidationServiceGroup = {
    ...RPCPayloadValidationBase,
    properties: {
        ...RPCPayloadValidationBase.properties,
        serviceGroup: { type: 'string', minLength: 1 },
    },
    required: [ 'serviceGroup', 'context', 'operation', 'payload' ],
};

const RPCPayloadValidationConfig : JSONSchemaType<RPCRequest> = {
    type: 'object',
    oneOf: [
        RPCPayloadValidationServiceName,
        RPCPayloadValidationServiceGroup,
    ],
    required: [ 'context', 'operation', 'payload' ],
};

class ValidationEngine
{
    readonly #validator;

    constructor()
    {
        const ajvConfig = { allErrors: true, $data: true, discriminator: true };
        this.#validator = new Ajv(ajvConfig);
        InstallAjvErrors.default(this.#validator);
        InstallAjvFormats.default(this.#validator);
        InstallAjvKeywords.default(this.#validator);
        this.#validator.addSchema(RPCPayloadValidationConfig, SCHEMA_NAME);
    }

    validateRPCRequest(request : RPCRequest) : string[]
    {
        const validate = this.#validator.getSchema(SCHEMA_NAME);
        const valid = validate(request);
        if(!valid)
        {
            const validationErrors = parseErrors(validate.errors);
            return _.castArray(validationErrors);
        }
        return [];
    }
}

export default new ValidationEngine();
