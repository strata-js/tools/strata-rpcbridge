import { RPCRequest, ServiceConfig } from '../interfaces/index.js';

interface ParsedServiceConfig
{
    serviceGroup : string;
    blockList : Map<string, Set<string>> | null;
    allowList : Map<string, Set<string>> | null;
}

interface RPCCallAllowedResult
{
    allowed : boolean;
    reason ?: 'ALLOW' | 'DENY' | 'GLOBAL_DENY' | 'NOT_CONFIGURED';
}

export class ConfigParserEngine
{
    #serviceMap = new Map<string, ParsedServiceConfig>();

    constructor(serviceConfig : ServiceConfig, globalBlockList : string[])
    {
        this.#parseConfig(serviceConfig, globalBlockList);
    }

    #parseList(list : string[]) : Map<string, Set<string>>
    {
        const tempMap = new Map<string, Set<string>>();

        for(const item of list)
        {
            const itemSplit = item.split(':');
            const context = itemSplit[0].toLowerCase();
            const operation = itemSplit[1].toLowerCase();
            if(!tempMap.has(context))
            {
                tempMap.set(context, new Set());
            }
            const tempSet = tempMap.get(context);
            if(tempSet) { tempSet.add(operation); }
        }
        return tempMap;
    }

    #parseConfig(serviceConfig : ServiceConfig, globalBlockList : string[]) : void
    {
        const serviceKeys = Object.keys(serviceConfig);
        this.#serviceMap.set(
            'global',
            {
                serviceGroup: '',
                blockList: globalBlockList ? this.#parseList(globalBlockList) : null,
                allowList: null,
            }
        );
        serviceKeys.forEach((keyName : string) =>
        {
            this.#serviceMap.set(
                keyName.toLowerCase(),
                {
                    serviceGroup: serviceConfig[keyName].serviceGroup,
                    blockList: serviceConfig[keyName].blockList
                        ? this.#parseList(serviceConfig[keyName].blockList as string[]) : null,
                    allowList: serviceConfig[keyName].allowList
                        ? this.#parseList(serviceConfig[keyName].allowList as string[]) : null,
                }
            );
        });
    }

    #inList(context : string, operation : string, list : Map<string, Set<string>>) : boolean
    {
        const operationSet = list.get(context.toLowerCase());
        if(!operationSet)
        {
            return false;
        }
        return operationSet.has('*') || operationSet.has(operation.toLowerCase());
    }

    lookupServiceGroup(request : RPCRequest) : string | null
    {
        const serviceConfig = this.#serviceMap.get(request.serviceName?.toLowerCase() ?? '');
        return serviceConfig?.serviceGroup ?? null;
    }

    isRPCCallAllowed(request : RPCRequest, allowUnconfigured = false) : RPCCallAllowedResult
    {
        const serviceConfig = this.#serviceMap.get(request.serviceName?.toLowerCase() ?? '');
        if(!(serviceConfig || allowUnconfigured))
        {
            return {
                allowed: false,
                reason: 'NOT_CONFIGURED',
            };
        }

        const globalConfig = this.#serviceMap.get('global');

        if(globalConfig && globalConfig.blockList)
        {
            if(this.#inList(request.context, request.operation, globalConfig.blockList))
            {
                return {
                    allowed: false,
                    reason: 'GLOBAL_DENY',
                };
            }
        }

        if(serviceConfig?.allowList)
        {
            if(!this.#inList(request.context, request.operation, serviceConfig.allowList))
            {
                return {
                    allowed: false,
                    reason: 'ALLOW',
                };
            }
        }
        else if(serviceConfig?.blockList)
        {
            if(this.#inList(request.context, request.operation, serviceConfig.blockList))
            {
                return {
                    allowed: false,
                    reason: 'DENY',
                };
            }
        }

        return { allowed: true };
    }
}
