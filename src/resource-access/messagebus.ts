import { ResponseEnvelope, StrataClient, StrataClientConfig } from '@strata-js/strata';

export class MessageBusRA
{
    #client : Promise<StrataClient>;
    #config : StrataClientConfig;

    constructor(config : StrataClientConfig)
    {
        this.#config = config;
        this.#client = this.#setClient();
    }

    // Make sure we don't make requests until the client is initialized.
    async #setClient() : Promise<StrataClient>
    {
        return new Promise((resolve, reject) =>
        {
            const _client = new StrataClient(this.#config);
            _client.start()
                .then(() =>
                {
                    resolve(_client);
                })
                .catch((err) =>
                {
                    reject(err);
                });
        });
    }

    async request<
        ReturnPayloadType = Record<string, unknown>,
        MetadataType extends Record<string, unknown> = Record<string, unknown>>(
        serviceGroup : string,
        context : string,
        operation : string,
        payload : Record<string, unknown>,
        metadata ?: MetadataType,
        auth ?: string,
        timeout ?: number
    ) : Promise<ResponseEnvelope<ReturnPayloadType>>
    {
        const _client = await this.#client;
        return _client.request<ReturnPayloadType, MetadataType>(
            serviceGroup,
            context,
            operation,
            payload,
            metadata,
            auth,
            timeout
        );
    }
}
