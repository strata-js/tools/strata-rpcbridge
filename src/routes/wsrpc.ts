import { Socket } from 'socket.io';
import { SocketRouter } from '../lib/socketrouter.js';
import { RPCBridgeManager } from '../managers/rpcbridge.js';
import { RPCRequest, RPCResponse, SocketMiddleware, WebServerConfig } from '../interfaces/index.js';
import * as ServerErrors from '../lib/error.js';
import { errors } from '@strata-js/strata';

async function executeMiddleware(func : SocketMiddleware, socket : Socket, rpcRequest : RPCRequest)
    : Promise<void>
{
    return new Promise((resolve, reject) =>
    {
        func(socket, rpcRequest, (errMsg ?: string) =>
        {
            if(errMsg)
            {
                reject(new ServerErrors.ServerError(errMsg, 'MIDDLEWARE_ERROR'));
            }
            resolve();
        });
    });
}

function createRouter(
    rpcBridgeManager : RPCBridgeManager,
    webServerConfig : WebServerConfig,
    middlewareList ?: SocketMiddleware[]
) : SocketRouter
{
    const router = new SocketRouter();

    router.event('rpc', async (_socket, request : RPCRequest, ...args : unknown[]) =>
    {
        const webSocketCallback = args.pop() as (response : RPCResponse) => void;
        if(typeof webSocketCallback !== 'function')
        {
            console.error(`A request without a callback was made.`);
            return;
        }

        if(args.length > 0)
        {
            console.error('malformed request');
            webSocketCallback({
                status: 'failed',
                response: new ServerErrors.MalformedWebSocketRequest().toJSON(webServerConfig.excludeStack),
            });
            return;
        }

        try
        {
            if(middlewareList)
            {
                for(const middleware of middlewareList)
                {
                    // eslint-disable-next-line no-await-in-loop
                    await executeMiddleware(middleware, _socket, request);
                }
            }
            const result = await rpcBridgeManager.request(request);
            webSocketCallback(result);
        }
        catch (err)
        {
            let errorResult : Record<string, unknown>;
            if(err instanceof ServerErrors.ServerError || err instanceof errors.ServiceError)
            {
                errorResult = err.toJSON(webServerConfig.excludeStack);
            }
            else
            {
                errorResult = {
                    message: 'Internal Service Error',
                };
            }
            webSocketCallback({
                status: 'failed',
                response: errorResult,
            });
        }
    });

    return router;
}

export default createRouter;
