import express, { Request, Response, Router } from 'express';
import { RPCBridgeManager } from '../managers/rpcbridge.js';
import { HTTPMiddleware, WebServerConfig } from '../interfaces/index.js';
import { RemoteServiceError, ServerError } from '../lib/error.js';
import { errors } from '@strata-js/strata';

function createRouter(
    rpcBridgeManager : RPCBridgeManager,
    webServerConfig : WebServerConfig,
    middlewareList ?: HTTPMiddleware[]
) : Router
{
    const router = express.Router();

    if(middlewareList)
    {
        middlewareList.forEach((middleware : HTTPMiddleware) =>
        {
            router.use('/', middleware);
        });
    }

    router.post('/', async (req : Request, res : Response) =>
    {
        try
        {
            const result = await rpcBridgeManager.request(req.body);
            if(result.status === 'failed')
            {
                throw new RemoteServiceError(result.response);
            }
            res.json(result.response);
        }
        catch (err)
        {
            if(err instanceof ServerError || err instanceof errors.ServiceError)
            {
                res.status(400).send(err.toJSON(webServerConfig.excludeStack));
            }
            else if(err instanceof Error)
            {
                res.status(400).send({ message: err.message });
            }
            else
            {
                res.status(400).send({ message: 'Internal Server Error' });
            }
            res.end();
        }
    });

    return router;
}

export default createRouter;
