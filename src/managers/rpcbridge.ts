import { MessageBusRA } from '../resource-access/messagebus.js';
import { RPCRequest, RPCResponse, ServerConfig } from '../interfaces/index.js';
import validationEngine from '../engines/validation.js';
import { ConfigParserEngine } from '../engines/configparse.js';
import * as ServerErrors from '../lib/error.js';
import { errors } from '@strata-js/strata';

export class RPCBridgeManager
{
    #configEngine : ConfigParserEngine;
    #serverConfig : ServerConfig;
    #messageBusRA : MessageBusRA;

    constructor(serverConfig : ServerConfig)
    {
        this.#serverConfig = serverConfig;
        this.#configEngine = new ConfigParserEngine(this.#serverConfig.services, this.#serverConfig.globalBlockList);
        this.#messageBusRA = new MessageBusRA(this.#serverConfig.strata);
    }

    async request(request : RPCRequest) : Promise<RPCResponse>
    {
        const validationResults = validationEngine.validateRPCRequest(request);
        if(validationResults.length > 0)
        {
            throw new ServerErrors.InvalidRequestPayload(validationResults);
        }
        if(request.serviceGroup && !this.#serverConfig.server.allowServiceGroupOverride)
        {
            throw new ServerErrors.InvalidRequestPayload([
                `Overriding the queue name is not allowed: ${ request.serviceGroup }`,
            ]);
        }

        const isAllowed = this.#configEngine.isRPCCallAllowed(
            request,
            this.#serverConfig.server.allowServiceGroupOverride
        );
        let serviceGroup : string | null = null;
        if(this.#serverConfig.server.allowServiceGroupOverride && request.serviceGroup)
        {
            serviceGroup = request.serviceGroup;
        }
        if(!serviceGroup)
        {
            serviceGroup = this.#configEngine.lookupServiceGroup(request);
        }

        if(!isAllowed.allowed || serviceGroup === null)
        {
            throw new ServerErrors.InvalidContextOperation(
                request.serviceName ?? request.serviceGroup ?? 'Unknown Service',
                request.context,
                request.operation
            );
        }

        try
        {
            const result = await this.#messageBusRA.request(
                serviceGroup,
                request.context,
                request.operation,
                request.payload,
                request?.meta,
                request?.auth,
                request?.timeout
            );
            return {
                status: 'success',
                response: result,
            };
        }
        catch (err)
        {
            let errorResult : Record<string, unknown>;
            if(err instanceof ServerErrors.ServerError || err instanceof errors.ServiceError)
            {
                errorResult = err.toJSON(this.#serverConfig.server.excludeStack);

                if(this.#serverConfig.server.excludeUnsafeMessages && !errorResult.isSafeMessage)
                {
                    errorResult.message = 'Internal Service Error';
                }
            }
            else
            {
                errorResult = {
                    message: 'Internal Service Error',
                };
            }

            return {
                status: 'failed',
                response: errorResult,
            };
        }
    }
}
