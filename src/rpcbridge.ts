import http from 'http';
import express, { Express } from 'express';
import { Server as SIOServer } from 'socket.io';

import { RPCBridgeManager } from './managers/rpcbridge.js';
import WSRouter from './routes/wsrpc.js';
import HTTPRouter from './routes/rpc.js';

import { MiddlewareConfig, ServerConfig } from './interfaces/index.js';

interface ExternalServerInstances
{
    expressApp : Express;
    httpServer : http.Server;
    ioServer : SIOServer;
}

export * from './interfaces/index.js';

export class RPCBridgeServer
{
    #serverConfig : ServerConfig;
    #rpcBridgeManager : RPCBridgeManager;
    #app : Express;
    #server : http.Server;
    #io : SIOServer;
    #middlewareConfig : MiddlewareConfig | undefined;

    constructor(
        serverConfig : ServerConfig,
        middlewareConfig ?: MiddlewareConfig,
        externalInstances ?: ExternalServerInstances
    )
    {
        this.#serverConfig = serverConfig;
        this.#middlewareConfig = middlewareConfig;
        this.#rpcBridgeManager = new RPCBridgeManager(this.#serverConfig);

        // TODO: Handle a more granular set of options.
        if(!externalInstances)
        {
            this.#app = express();
            this.#server = http.createServer(this.#app);
            this.#io = new SIOServer(this.#server);
        }
        else
        {
            this.#app = externalInstances.expressApp;
            this.#server = externalInstances.httpServer;
            this.#io = externalInstances.ioServer;
        }
        this.#initServer();
    }

    #initHTTPListener() : void
    {
        const httpRouter = HTTPRouter(this.#rpcBridgeManager, this.#serverConfig.server, this.#middlewareConfig?.http);
        this.#app.use(this.#serverConfig.server.path, express.json(), httpRouter);
    }

    #initWebSocketListener() : void
    {
        const wsRouter = WSRouter(this.#rpcBridgeManager, this.#serverConfig.server, this.#middlewareConfig?.socket);
        this.#io.of(this.#serverConfig.server.path).on('connection', (socket) =>
        {
            socket.onAny(wsRouter.handler(socket));
        });
    }

    #initServer() : void
    {
        if(this.#serverConfig.server.enableHTTP)
        {
            this.#initHTTPListener();
        }

        if(this.#serverConfig.server.enableWS)
        {
            this.#initWebSocketListener();
        }
    }

    startListening() : void
    {
        this.#server.listen(this.#serverConfig.server.port);
        console.log(`Server is listening on ${ this.#serverConfig.server.port }`);
    }
}
