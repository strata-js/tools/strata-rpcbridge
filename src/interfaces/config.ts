import { StrataClientConfig } from '@strata-js/strata';

export interface ServiceEntry
{
    serviceGroup : string;
    blockList ?: string[];
    allowList ?: string[];
}

export type ServiceConfig = Record<string, ServiceEntry>;

export interface WebServerConfig
{
    enableHTTP : boolean;
    enableWS : boolean;
    port : number;
    path : string;
    excludeStack : boolean;
    excludeUnsafeMessages : boolean;
    allowServiceGroupOverride ?: boolean;
}

export interface ServerConfig
{
    server : WebServerConfig;
    strata : StrataClientConfig;
    globalBlockList : string[];
    services : ServiceConfig;
}
