export interface RPCResponse
{
    status : 'success' | 'failed';
    response : any;
}
