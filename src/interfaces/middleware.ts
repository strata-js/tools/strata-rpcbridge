import { Request, Response } from 'express';
import { Socket } from 'socket.io';
import { RPCRequest } from './request.js';

export type HTTPMiddleware = (req : Request, res : Response, next : () => void) => void;
export type SocketMiddleware = (socket : Socket, rpcRequest : RPCRequest, next : (errMsg ?: string) => void) => void;

export interface MiddlewareConfig
{
    http ?: HTTPMiddleware[];
    socket ?: SocketMiddleware[];
}
