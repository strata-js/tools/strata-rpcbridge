import { MarkRequired, XOR } from 'ts-essentials';

interface RPCRequestBase
{
    serviceName ?: string;
    context : string;
    operation : string;
    payload : Record<string, any>;
    meta ?: Record<string, any>;
    auth ?: string;
    timeout ?: number;
    serviceGroup ?: string;
}

// Either serviceName or serviceGroup is required, the other is optional in both cases
export type RPCRequest = XOR<MarkRequired<RPCRequestBase, 'serviceName'>, MarkRequired<RPCRequestBase, 'serviceGroup'>>;
